<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SANBERCODE BOOTCAMP</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name:</label> <br />
        <input type="text" id="fname" name="fname" /><br /><br />
        <label for="lname">Last name:</label> <br />
        <input type="text" id="lname" name="lname" /><br /><br />
        <label for="gender">Gender:</label> <br />
        <input type="radio" id="male" name="gender_type" value="Male" />
        <label for="male">Male</label><br />
        <input type="radio" id="female" name="gender_type" value="Female" />
        <label for="female">Female</label><br />
        <input type="radio" id="female" name="gender_type" value="Other" />
        <label for="female">Other</label><br /><br />
        <label for="nationality">Nationality:</label><br /><br />
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesian</option>
            <option value="malaysia">Malaysian</option>
            <option value="thailand">Thailand</option>
        </select><br /><br />
        <label for="language">Language Spoken:</label><br /><br />
        <input type="checkbox" id="lang1" name="lang1" value="indonesia" />
        <label for="lang1">Bahasa Indonesia</label><br />
        <input type="checkbox" id="lang2" name="lang2" value="english" />
        <label for="lang2">English</label><br />
        <input type="checkbox" id="lang3" name="lang3" value="other" />
        <label for="lang3">Other</label><br /><br />
        <label for="bio">Bio:</label><br /><br />
        <textarea id="bioarea" name="bioarea" rows="4" cols="20"> </textarea><br /><br />
        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>