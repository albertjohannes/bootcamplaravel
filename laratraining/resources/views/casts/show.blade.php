@extends('layouts.master')

@section('title')
Detail {{$casts->nama}}
@endsection

@section('content')

<h1>{{$casts->nama}}</h1>
<h2>{{$casts->umur}}</h2>
<p>{{$casts->bio}}</p>

@endsection