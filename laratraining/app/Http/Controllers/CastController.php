<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('casts.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required|max:11',
            'bio' => 'required',
        ]);



        $query = DB::table('casts')->insert([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"],
            'created_at' => now(),
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('casts.index', compact('casts'));
    }

    public function show($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        // dd($casts);
        return view('casts.show', compact('casts'));
    }

    public function edit($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        // dd($casts);
        return view('casts.edit', compact('casts'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required|max:11',
            'bio' => 'required',
        ]);

        $affected = DB::table('casts')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                    'updated_at' => now()
                ]
            );

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
