<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        // dd($request);
        $fullname = $request->fname . ' ' . $request->lname;
        $nationality = $request['nationality'];
        // dd($fullname, $nationality);

        return view('welcome', compact('fullname', 'nationality'));
    }
}
